# Jade's Blender -> Unity Rotation Fix Script 
Tl;dr To run, execute/double click
> BlenderRotationFix.command

This script copies the latest Unity-BlenderToFBX.py into Unity's program files. It applies the change to all Unity Versions currently installed using the Unity Hub on MacOS. 

---
### This script does the following:
1. Checks if all the folders it needs exist
2. Pulls the latest version of the rotation script from https://gist.github.com/MattRix/122f258ba6eaa23dd9c5
3. Copies the rotation fix into all currently installed Unity versions.
4. Checks that the copy was successful (via cmp)

---
### In the event the external Unity-BlenderToFBX.py repo was deleted: 
(In case MattRix deletes https://gist.github.com/MattRix/122f258ba6eaa23dd9c5)
- Unzip Unity-BlenderToFBX-Backup.py.zip into this repo's ScriptGistRepo/Unity-BlenderToFBX.py
- run the script again

