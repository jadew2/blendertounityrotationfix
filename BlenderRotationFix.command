#!/bin/bash

#Echo color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
NC='\033[0m' # No Color

cd "$(dirname ${BASH_SOURCE[0]})"

if [[ $EUID > 0 ]]; then
    echo -e "${YELLOW}Please enter the sudo password${NC}"
    echo -e "${YELLOW}I need sudo to copy files to Unity's install location${NC}"
    echo "Trying to re-run the script as sudo..."
    exec sudo "$0" "$@"
    exit 0
fi

if [ "$UID" -eq 1 ]; then
	echo -e "${RED}Sudo verification failed... Try again.${NC}"
	exit 1
fi

# Unity has python script to import Blender .fbx files, we’ll just replace it with this better version ;)
# Check for updates: http://blenderartists.org/forum/showthread.php?255246-Rotate-whole-scene-around-x-90-and-apply-rotation/page2
echo " "
echo "--- Blender Rotation Script ---"
echo -e "Would you like to fix the blender import script?"
echo -e "This will apply to every Unity version you installed."
read -p "[y/n]" yn

if [ "$yn" != "y" ]; then
    echo "no, ok terminating."
    exit 0
fi

#1. Check if the unity app path is good...
unityAppPath="/Applications/Unity/Hub/Editor"
if [ ! -d "$unityAppPath" ]; then
    echo " "
    echo -e "${RED}Looks like \"$unityAppPath\" does not exist.${NC}"
    echo -e "${RED}Maybe the Unity Hub is installing to somewhere else now.${NC}"
    echo -e "${RED}Please change the directory in the script.${NC}"
    exit 1
fi

#2. Check if the ScriptGistRepo/Unity-BlenderToFBX.py exists
scriptGistRepo="ScriptGistRepo"
if [ ! -d "$scriptGistRepo" ]; then
    echo " "
    echo -e "${RED}Looks like the git submodule folder \"$scriptGistRepo\" does not exist?${NC}"
    echo -e "${RED}Did you delete it? Maybe do a git reset --hard pls.${NC}"
    echo -e "${RED}Or if they deleted that remote repo, unzip the Unity-BlenderToFBX-Backup.py.zip and make a path like \"$scriptGistRepo/Unity-BlenderToFBX.py\" then run this script again.${NC}"
    exit 1
fi

#3.  Pull the latest version of the script 
pushd $scriptGistRepo > /dev/null
echo "Pulling latest version..."
git pull
popd > /dev/null

newScriptPath="$scriptGistRepo/Unity-BlenderToFBX.py"
if [ ! -f "$newScriptPath" ]; then
    echo " "
    echo -e "${RED}Looks like the script file \"$newScriptPath\" does not exist?${NC}"
    echo -e "${RED}Did you delete it? Maybe do a git reset --hard pls.${NC}"
    echo -e "${RED}Or if they deleted that remote repo, unzip the Unity-BlenderToFBX-Backup.py.zip and make a path like \"$scriptGistRepo/Unity-BlenderToFBX.py\" then run this script again.${NC}"
    exit 1
fi

#4. Gather a list of all the unity versions
pushd $unityAppPath > /dev/null
unityInstallDirectories=$(ls | grep -v .DS_Store) #LS and remove .DS_Store from the results
popd > /dev/null

#5. Copy the script to the locations
for directory in $unityInstallDirectories; do
    oldScriptPath="$unityAppPath/$directory/Unity.app/Contents/Tools/Unity-BlenderToFBX.py"    
    
    echo -n -e "${GREEN}Copying "
    cp -v "$newScriptPath" "$oldScriptPath"
    echo -n -e "${NC}"
    
    if cmp -s "$newScriptPath" "$oldScriptPath"; then
	    echo -e "${GREEN} Copy Comparison Ok!${NC}"
    else
        echo -e "${RED} Copy failed; Files were different. :( Please try again...${NC}"
    fi
done

read -p "All done! Press enter to close this window."
echo Done
